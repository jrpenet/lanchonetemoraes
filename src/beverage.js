import {addItemTbl, addMenuBebidas} from './menuDrinks'

addItemTbl()
addMenuBebidas()

document.querySelector('.goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})

document.querySelector('.btn-avancaForm').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('././txentrega.html')
})