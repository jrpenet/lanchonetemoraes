let clientes = []

const loadClientes = function(){
    const clientesJSON = sessionStorage.getItem('clientes')
    
    if(clientesJSON !== null){
        return JSON.parse(clientesJSON)
    } else {
        return []
    }
}

const saveClientes = function(){
    sessionStorage.setItem('clientes', JSON.stringify(clientes))
}

//expose orders from module
const getClientes = () => clientes

//funcao para add clientes
const insereClientes = (nome, end, bairro, cel, pRef, pg, troco, obs) => {
    clientes.unshift(nome, end, bairro, cel, pRef, pg, troco, obs)
    saveClientes()
}

//funcao para limpar registro do cliente
const removeClientes = () => {
    sessionStorage.clear()
}

clientes = loadClientes()

export {getClientes, insereClientes, removeClientes, saveClientes}