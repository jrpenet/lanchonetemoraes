import {criaPedidos} from './pedidos'

//gera o menu na tela

//cardapio da tela
const cardapio = [{
    nomeHamburguer: 'Clone Basiquinho',
    descricao: 'Pão Bola, carne artesanal 100g cada, ovo, queijo, salada e molho da casa.',
    preco: 22,
    foto: './images/basiquinho.png'
}, {
    nomeHamburguer: 'Clone Simples',
    descricao: 'Pão Bola, ovo, queijo, calabresa, bacon, salada e molho da casa.',
    preco: 30,
    foto: './images/simples.png'
}, {
    nomeHamburguer: 'Clone de Frango',
    descricao: 'Pão Bola, filé de peito de frango, ovo, queijo, calabresa, bacon, salada e molho da casa.',
    preco: 30,
    foto: './images/geraldao.png'
}, {
    nomeHamburguer: 'Clone Nivaldão',
    descricao: 'Pão Bola, 2 carnes artesanais 100g cada, ovo, queijo, calabresa, bacon, salada e molho da casa.',
    preco: 35,
    foto: './images/nivaldao.png'
}, {
    nomeHamburguer: 'Clone X-Tudinho',
    descricao: 'Pão Bola, 2 carnes artesanais 100g cada, filé de peito de frango, ovo, queijo, calabresa, bacon, salada e molho da casa.',
    preco: 38,
    foto: './images/xtudinho.png'
}, {
    nomeHamburguer: 'Clone Geraldão',
    descricao: 'Pão Bola, 2 carnes artesanais 100g cada, peito de frango, calabresa, bacon, cheddar, salada, catupiry, salsichinha e molho da casa.',
    preco: 41,
    foto: './images/xtudinho.png'
}]

// , {
//     nomeHamburguer: 'Cabo Moraes',
//     descricao: 'Pão Bola, 4 carnes artesanais 100g cada, filé de peito de frango, ovo, queijo, calabresa, bacon, presunto, salcichinha à cabidela, salada e molho da casa.',
//     preco: 33,
//     foto: './images/cabomoraes.png'
// }

const addElements = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        subValor.textContent = `R$ ${e.target.value * lanche.preco},00`
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        criaPedidos(qt, prodt, price)
        location.assign('./beverage.html')
    })

    return divMain
}

const addCardapio = () => {
    cardapio.forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docTable').appendChild(hrEl)
        document.querySelector('#docTable').appendChild(addElements(lanche))
        
    })
}




export {addCardapio, addElements}